// console.log("Hello JSON!")

// [SECTION] JSON OBJECTS
	
	/*
	 	- JSON stands for JS object notation. Can also be use in other programming language.

	 	- core JS has a built in JSON object that contains methods for parsing JSON objects and converting strings into JS objects.

	 	- JSON is used for serializing different data types.

	 	- Syntax:
	 		{
				"propertyA": "valueA"
				"propertyB": "valueB"
	 		}

	 */

// JSON Object example
	/*
	{
		"city" : "QC"
		"province" : "Metro Manila"
		"country" : "PH"
	}
	*/

// JSON Arrays example
	/*
	[
		{
			"city" : "QC", 
			"province" : "Metro Manila", 
			"country" : "PH"
			},
		{
			"city" : " Manila", 
			"province" : "Metro Manila", 
			"country" : "PH"
			}
	]
	*/


// [SECTION] JSON METHOD

	/*
		- JSON object contains method for parsing and converting data into stringified JSON
	*/

	// Stringify Method - is used to convert JS objects into a string

	let batchArr = [
					{
						batchName: "Batch X"
						},
					{
						batchName: "Batch Y"
						}
					];

	console.log("This is the original array:");
	console.log(batchArr);

	console.log("Stringify result:");
	let stringBatchArr = JSON.stringify(batchArr);
	console.log(stringBatchArr);
	console.log(typeof stringBatchArr);

	// another data 
	let data = JSON.stringify(
	{
		name: 'John',
		address:{
			city: 'Manila',
			country: 'PH'
		}
	})
	console.log(data)


// [SECTION] STRINGIFY MWTHOD WITH VARIABLES

	// let firstName = prompt("What is your first name?");
	// let lastName = prompt("What is your last name?");
	// let age = prompt("How young are you?");
	// let address = {
	// 	city: prompt("Which city do you live in?"),
	// 	country: prompt ("Which country are you from?")
	// }

	// let otherData = JSON.stringify({
	// 	firstName: firstName,
	// 	lastName: lastName,
	// 	age: age,
	// 	address: address
	// })

	// if property name = variable name, we can ommit the value name
	// let otherData = JSON.stringify({
	// 	firstName,
	// 	lastName,
	// 	age,
	// 	address
	// })

	// console.log(otherData)


// [SECTION] CONVERTING STRINGIFIED JASON INTO JS OBJECT
	/*
		- Objects are common data type used in app becaise of the complez structures that can be created out of them.
	*/

	// let batchesJSON = `[{"batchName":"Batch X"},{"batchName":"Batch Y"}]`;
	// let parsedBatchJson = JSON.stringify(batchesJSON);
	// console.log(parsedBatchJson)

	// console.log(parsedBatchJson[0])


	let stringifiedObject = `{
			"name" : "John",
			"age" : "31",
			"address" : {
				"city" : "Manila",
				"country" : "PH"
				}
			}`